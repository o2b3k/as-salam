package ru.cyberblogs.as_salam;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import ru.cyberblogs.as_salam.ViewContent;
/**
 * Created by O2B3K on 09.02.2017.
 */
public class AllCategory extends AppCompatActivity{
    int count = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category);

    }

    @Override
    public void onBackPressed(){
        count++;
        setContentView(R.layout.category);
        if (count > 1){
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
        }
    }

    public void oltinchi(View view){
        Intent intent = new Intent(this,ViewContent.class);
        startActivity(intent);
    }

}
