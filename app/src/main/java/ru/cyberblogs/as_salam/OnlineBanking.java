package ru.cyberblogs.as_salam;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by O2B3K on 09.02.2017.
 */

public class OnlineBanking extends AppCompatActivity{
    WebView webView;
    int count = 0;
    String url = "https://www.salamkwt.org/index.php/ar/guest-entry-ar/?lang=ar&pay=true&cs_id=1234";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.banking);
        webView = (WebView) findViewById(R.id.banking);
        webView.loadUrl(url);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setWebViewClient(new MyWebViewClient());
    }

    @Override
    public void onBackPressed() {
        count++;
        if(webView.canGoBack()) {
            webView.goBack();
        } else {
            if (count > 1){
                Intent intent = new Intent(this,MainActivity.class);
                startActivity(intent);
            };
        }
    }
}

class MyWebViewClient extends WebViewClient
{
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url)
    {
        view.loadUrl(url);
        return true;
    }
}
