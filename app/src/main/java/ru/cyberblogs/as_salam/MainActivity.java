package ru.cyberblogs.as_salam;

/*
*  Bismillaxir Roxmanir Rohiym
*  Bakhriddin Bakhramov
* */

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.boshmenu);
    }

    public void onCategory(View view){
        Intent intent = new Intent(this,AllCategory.class);
        startActivity(intent);
    }

    public void onBanking(View view){
        Intent intent = new Intent(this,OnlineBanking.class);
        startActivity(intent);
    }
}